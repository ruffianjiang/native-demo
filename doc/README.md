

## 报错

```shell
Error: Failed to find 'vcvarsall.bat' in a Visual Studio installation.
```


1. 在path中添加你vs目录下msvc的bin目录

PATH
```shell
D:\app\software\Microsoft Visual Studio\2022\Community\VC\Tools\MSVC\14.37.32822\bin\Hostx64\x64
```



2. 接下来需要配置环境变量INCLUDE和LIB

INCLUDE
```shell
D:\Windows Kits\10\Include\10.0.22000.0\ucrt;
D:\Windows Kits\10\Include\10.0.22000.0\um;
D:\Windows Kits\10\Include\10.0.22000.0\shared;
D:\Program Files\Microsoft Visual Studio\2022\Enterprise\VC\Tools\MSVC\14.35.32215\include;
```


LIB
```shell
D:\Windows Kits\10\Lib\10.0.22000.0\um\x64;
D:\Windows Kits\10\Lib\10.0.22000.0\ucrt\x64;
D:\Program Files\Microsoft Visual Studio\2022\Enterprise\VC\Tools\MSVC\14.35.32215\lib\x64;
```



3. 安装 [anaconda](https://www.anaconda.com/) 

执行安装
> conda install libpython

4. 重启电脑

