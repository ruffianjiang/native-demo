# spring native在windows环境下的打包

https://www.yuque.com/uu200/lknb31/fx0ex10lxgierch3


## 使用环境
> windows11:  22631.2361  
jdk: graalvm-jdk-17.0.8+9.1  
visaul stadio 2022: 17.7.4  
springboot: 3.1.4  

## 打包环境准备
#### 安装jdk17

1、到oracle官网下载GraalVM for JDK 17 ，windows版本，点击安装
[https://www.oracle.com/java/technologies/downloads/#graalvmjava17-windows](https://www.oracle.com/java/technologies/downloads/#graalvmjava17-windows)

2、配置环境变量
**GRAALVM_HOME**
> C:\Program Files\Java\graalvm-jdk-17.0.8+9.1

**JAVA_HOME**
> C:\Program Files\Java\graalvm-jdk-17.0.8+9.1

**Path**
> C:\Program Files\Java\graalvm-jdk-17.0.8+9.1\bin


3、打开命令行，输入**gu.cmd list**，确保有 **graalvm **和 **native-image ，**输入**java -version，**查看java的版本是不是17的。
![企业微信截图_16967546442573.png](doc/img/企业微信截图_16967546442573.png)

注：可以通过gu.cmd available查看可下载的资源，gu.cmd install native-image下载


#### 安装visaul stadio

1、下载Visual Studio 2022社区版
[https://visualstudio.microsoft.com/zh-hans/downloads/](https://visualstudio.microsoft.com/zh-hans/downloads/)

2、安装使用c++的桌面开发，以及一些单个组件

![企业微信截图_16967570632547.png](doc/img/企业微信截图_16967570632547.png)

![企业微信截图_16967570991606.png](doc/img/企业微信截图_16967570991606.png)


3、配置环境变量
**PATH**
> D:\app\software\Microsoft Visual  Studio\2022\Community\VC\Tools\MSVC\14.37.32822\bin\Hostx64\x64


**INCLUDE**
> D:\Windows Kits\10\Include\10.0.22621.0\ucrt;
> D:\Windows Kits\10\Include\10.0.22621.0\um;
> D:\Windows Kits\10\Include\10.0.22621.0\shared;
> D:\app\software\Microsoft Visual Studio\2022\Community\VC\Tools\MSVC\14.37.32822\include;


**LIB**
> D:\Windows Kits\10\Lib\10.0.22621.0\um\x64;
> D:\Windows Kits\10\Lib\10.0.22621.0\ucrt\x64;
> D:\app\software\Microsoft Visual Studio\2022\Community\VC\Tools\MSVC\14.37.32822\lib\x64;

![企业微信截图_16967588237197.png](doc/img/企业微信截图_16967588237197.png)

![企业微信截图_16967588679070.png](doc/img/企业微信截图_16967588679070.png)

**4、重启电脑**

#### 项目验证
1、新建一个spring boot3的项目，pom文件如下
```xml
<?xml version="1.0" encoding="UTF-8"?>
<project xmlns="http://maven.apache.org/POM/4.0.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
         xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 https://maven.apache.org/xsd/maven-4.0.0.xsd">
    <modelVersion>4.0.0</modelVersion>
    <parent>
        <groupId>org.springframework.boot</groupId>
        <artifactId>spring-boot-starter-parent</artifactId>
        <version>3.1.4</version>
        <relativePath/> <!-- lookup parent from repository -->
    </parent>
    <groupId>com.example</groupId>
    <artifactId>native-demo</artifactId>
    <version>0.0.1-SNAPSHOT</version>
    <name>native-demo</name>
    <description>native-demo</description>
    <properties>
        <java.version>17</java.version>
    </properties>
    <dependencies>
        <dependency>
            <groupId>org.springframework.boot</groupId>
            <artifactId>spring-boot-starter-actuator</artifactId>
        </dependency>
        <dependency>
            <groupId>org.springframework.boot</groupId>
            <artifactId>spring-boot-starter-thymeleaf</artifactId>
        </dependency>
        <dependency>
            <groupId>org.springframework.boot</groupId>
            <artifactId>spring-boot-starter-web</artifactId>
        </dependency>

        <dependency>
            <groupId>org.springframework.boot</groupId>
            <artifactId>spring-boot-starter-test</artifactId>
            <scope>test</scope>
        </dependency>
    </dependencies>

    <build>
        <plugins>
            <plugin>
                <groupId>org.springframework.boot</groupId>
                <artifactId>spring-boot-maven-plugin</artifactId>
            </plugin>
        </plugins>
    </build>

    <profiles>
        <profile>
            <!-- Enable this profile to build an image on AMD64 and ARM64 platforms -->
            <!-- until Paketo Buildpacks gets official support. -->
            <id>native</id>
            <build>
                <plugins>
                    <plugin>
                        <groupId>org.graalvm.buildtools</groupId>
                        <artifactId>native-maven-plugin</artifactId>
                        <configuration>
                            <buildArgs>--no-fallback</buildArgs>
                        </configuration>
                        <executions>
                            <execution>
                                <id>build-image</id>
                                <goals>
                                    <goal>compile-no-fork</goal>
                                </goals>
                                <phase>package</phase>
                            </execution>
                        </executions>
                    </plugin>
                    <plugin>
                        <groupId>org.springframework.boot</groupId>
                        <artifactId>spring-boot-maven-plugin</artifactId>
                        <configuration>
                            <image>
                                <builder>dashaun/builder:tiny</builder>
                            </image>
                        </configuration>
                    </plugin>
                </plugins>
            </build>
        </profile>
    </profiles>

</project>

```

2、打包
![企业微信截图_16967549656026.png](doc/img/企业微信截图_16967549656026.png)

部分打包日志如下，打包阶段，耗时较长，占用内存较大。

```verilog
========================================================================================================================
GraalVM Native Image: Generating 'native-demo' (executable)...
========================================================================================================================
For detailed information and explanations on the build output, visit:
https://github.com/oracle/graal/blob/master/docs/reference-manual/native-image/BuildOutput.md
------------------------------------------------------------------------------------------------------------------------
[1/8] Initializing...                                                                                    (8.4s @ 0.14GB)
 Java version: 17.0.8+9-LTS, vendor version: Oracle GraalVM 17.0.8+9.1
 Graal compiler: optimization level: 2, target machine: x86-64-v3, PGO: ML-inferred
 C compiler: cl.exe (microsoft, x64, 19.37.32824)
 Garbage collector: Serial GC (max heap size: 80% of RAM)
 1 user-specific feature(s)
 - org.springframework.aot.nativex.feature.PreComputeFieldFeature
Field org.apache.commons.logging.LogAdapter#log4jSpiPresent set to true at build time
Field org.apache.commons.logging.LogAdapter#log4jSlf4jProviderPresent set to true at build time
Field org.apache.commons.logging.LogAdapter#slf4jSpiPresent set to true at build time
Field org.apache.commons.logging.LogAdapter#slf4jApiPresent set to true at build time
Field org.springframework.web.servlet.config.annotation.WebMvcConfigurationSupport#romePresent set to false at build time
Field org.springframework.web.servlet.config.annotation.WebMvcConfigurationSupport#jaxb2Present set to false at build time
Field org.springframework.web.servlet.config.annotation.WebMvcConfigurationSupport#jackson2Present set to true at build time
Field org.springframework.web.servlet.config.annotation.WebMvcConfigurationSupport#jackson2XmlPresent set to false at build time
Field org.springframework.web.servlet.config.annotation.WebMvcConfigurationSupport#jackson2SmilePresent set to false at build time
Field org.springframework.web.servlet.config.annotation.WebMvcConfigurationSupport#jackson2CborPresent set to false at build time
Field org.springframework.web.servlet.config.annotation.WebMvcConfigurationSupport#gsonPresent set to false at build time
Field org.springframework.web.servlet.config.annotation.WebMvcConfigurationSupport#jsonbPresent set to false at build time
Field org.springframework.web.servlet.config.annotation.WebMvcConfigurationSupport#kotlinSerializationCborPresent set to false at build time
Field org.springframework.web.servlet.config.annotation.WebMvcConfigurationSupport#kotlinSerializationJsonPresent set to false at build time
Field org.springframework.web.servlet.config.annotation.WebMvcConfigurationSupport#kotlinSerializationProtobufPresent set to false at build time
Field org.springframework.http.converter.json.Jackson2ObjectMapperBuilder#jackson2XmlPresent set to false at build time
Field org.springframework.core.NativeDetector#inNativeImage set to true at build time
Field org.springframework.boot.logging.log4j2.Log4J2LoggingSystem$Factory#PRESENT set to false at build time
Field org.springframework.boot.logging.logback.LogbackLoggingSystem$Factory#PRESENT set to true at build time
Field org.springframework.web.servlet.view.InternalResourceViewResolver#jstlPresent set to false at build time
Field org.springframework.web.client.RestTemplate#romePresent set to false at build time
Field org.springframework.web.client.RestTemplate#jaxb2Present set to false at build time
Field org.springframework.web.client.RestTemplate#jackson2Present set to true at build time
Field org.springframework.web.client.RestTemplate#jackson2XmlPresent set to false at build time
Field org.springframework.web.client.RestTemplate#jackson2SmilePresent set to false at build time
Field org.springframework.web.client.RestTemplate#jackson2CborPresent set to false at build time
Field org.springframework.web.client.RestTemplate#gsonPresent set to false at build time
Field org.springframework.web.client.RestTemplate#jsonbPresent set to false at build time
Field org.springframework.web.client.RestTemplate#kotlinSerializationCborPresent set to false at build time
Field org.springframework.web.client.RestTemplate#kotlinSerializationJsonPresent set to false at build time
Field org.springframework.web.client.RestTemplate#kotlinSerializationProtobufPresent set to false at build time
Field org.springframework.boot.logging.java.JavaLoggingSystem$Factory#PRESENT set to true at build time
Field org.springframework.core.KotlinDetector#kotlinPresent set to false at build time
Field org.springframework.core.KotlinDetector#kotlinReflectPresent set to false at build time
Field org.springframework.cglib.core.AbstractClassGenerator#inNativeImage set to true at build time
Field org.springframework.aot.AotDetector#inNativeImage set to true at build time
Field org.springframework.web.context.support.StandardServletEnvironment#jndiPresent set to true at build time
Field org.springframework.format.support.DefaultFormattingConversionService#jsr354Present set to false at build time
Field org.springframework.boot.logging.logback.LogbackLoggingSystemProperties#JBOSS_LOGGING_PRESENT set to false at build time
Field org.springframework.context.event.ApplicationListenerMethodAdapter#reactiveStreamsPresent set to false at build time
Field org.springframework.web.context.support.WebApplicationContextUtils#jsfPresent set to false at build time
Field org.springframework.web.context.request.RequestContextHolder#jsfPresent set to false at build time
Field org.springframework.http.converter.support.AllEncompassingFormHttpMessageConverter#jaxb2Present set to false at build time
Field org.springframework.http.converter.support.AllEncompassingFormHttpMessageConverter#jackson2Present set to true at build time
Field org.springframework.http.converter.support.AllEncompassingFormHttpMessageConverter#jackson2XmlPresent set to false at build time
Field org.springframework.http.converter.support.AllEncompassingFormHttpMessageConverter#jackson2SmilePresent set to false at build time
Field org.springframework.http.converter.support.AllEncompassingFormHttpMessageConverter#gsonPresent set to false at build time
Field org.springframework.http.converter.support.AllEncompassingFormHttpMessageConverter#jsonbPresent set to false at build time
Field org.springframework.http.converter.support.AllEncompassingFormHttpMessageConverter#kotlinSerializationCborPresent set to false at build time
Field org.springframework.http.converter.support.AllEncompassingFormHttpMessageConverter#kotlinSerializationJsonPresent set to false at build time
Field org.springframework.http.converter.support.AllEncompassingFormHttpMessageConverter#kotlinSerializationProtobufPresent set to false at build time
Field org.springframework.core.ReactiveAdapterRegistry#reactorPresent set to false at build time
Field org.springframework.core.ReactiveAdapterRegistry#rxjava3Present set to false at build time
Field org.springframework.core.ReactiveAdapterRegistry#kotlinCoroutinesPresent set to false at build time
Field org.springframework.core.ReactiveAdapterRegistry#mutinyPresent set to false at build time
Field org.springframework.boot.autoconfigure.web.format.WebConversionService#JSR_354_PRESENT set to false at build time
Field org.springframework.boot.actuate.endpoint.invoker.cache.CachingOperationInvoker#IS_REACTOR_PRESENT set to false at build time
Field org.springframework.boot.actuate.endpoint.web.annotation.DiscoveredWebOperation#REACTIVE_STREAMS_PRESENT set to false at build time
SLF4J: No SLF4J providers were found.
SLF4J: Defaulting to no-operation (NOP) logger implementation
SLF4J: See https://www.slf4j.org/codes.html#noProviders for further details.
Field org.springframework.web.servlet.mvc.method.annotation.ReactiveTypeHandler#isContextPropagationPresent set to false at build time
Field org.springframework.boot.actuate.endpoint.invoke.reflect.OperationMethodParameter#jsr305Present set to false at build time
Field org.springframework.web.servlet.support.RequestContext#jstlPresent set to false at build time
[2/8] Performing analysis...  [******]                                                                  (28.8s @ 1.56GB)
  17,689 (90.97%) of 19,445 types reachable
  29,130 (68.55%) of 42,497 fields reachable
  94,376 (66.08%) of 142,824 methods reachable
   5,590 types,   281 fields, and 6,368 methods registered for reflection
      83 types,    78 fields, and    68 methods registered for JNI access
       5 native libraries: crypt32, ncrypt, psapi, version, winhttp
[3/8] Building universe...                                                                               (4.3s @ 1.50GB)
[4/8] Parsing methods...      [***]                                                                      (9.5s @ 1.62GB)
[5/8] Inlining methods...     [****]                                                                     (1.6s @ 1.34GB)
[6/8] Compiling methods...    [*********]                                                               (82.4s @ 1.83GB)
[7/8] Layouting methods...    [***]                                                                      (7.3s @ 2.03GB)
[8/8] Creating image...       [***]                                                                      (5.2s @ 1.17GB)
  54.94MB (61.05%) for code area:    53,796 compilation units
  34.32MB (38.14%) for image heap:  431,115 objects and 176 resources
 752.28kB ( 0.82%) for other data
  89.99MB in total
------------------------------------------------------------------------------------------------------------------------
Top 10 origins of code area:                                Top 10 object types in image heap:
  17.24MB java.base                                           11.59MB byte[] for code metadata
   5.39MB tomcat-embed-core-10.1.13.jar                        4.28MB byte[] for java.lang.String
   5.28MB svm.jar (Native Image)                               3.29MB java.lang.Class
   4.40MB java.xml                                             3.23MB java.lang.String
   2.58MB jackson-databind-2.15.2.jar                          2.92MB byte[] for general heap data
   2.31MB spring-core-6.0.12.jar                               1.35MB byte[] for embedded resources
   2.23MB thymeleaf-3.1.2.RELEASE.jar                          1.17MB byte[] for reflection metadata
   1.89MB spring-boot-3.1.4.jar                              829.17kB com.oracle.svm.core.hub.DynamicHubCompanion
 930.01kB spring-web-6.0.12.jar                              505.84kB c.o.svm.core.hub.DynamicHub$ReflectionMetadata
 916.99kB spring-beans-6.0.12.jar                            452.18kB java.lang.String[]
  11.45MB for 98 more packages                                 4.05MB for 3539 more object types
------------------------------------------------------------------------------------------------------------------------
Recommendations:
 PGO:  Use Profile-Guided Optimizations ('--pgo') for improved throughput.
 HEAP: Set max heap for improved and more predictable memory usage.
 CPU:  Enable more CPU features with '-march=native' for improved performance.
 QBM:  Use the quick build mode ('-Ob') to speed up builds during development.
 BRPT: Try out the new build reports with '-H:+BuildReport'.
------------------------------------------------------------------------------------------------------------------------
                       33.6s (22.5% of total time) in 448 GCs | Peak RSS: 2.97GB | CPU load: 8.90
------------------------------------------------------------------------------------------------------------------------
Produced artifacts:
 ...\native-demo\target\native-demo.exe (executable)
========================================================================================================================
Finished generating 'native-demo' in 2m 28s.
```

打包结果如下
![企业微信截图_16967554555378.png](doc/img/企业微信截图_16967554555378.png)
![企业微信截图_16967556898052.png](doc/img/企业微信截图_16967556898052.png)

3、启动

原生启动，0.098s
![企业微信截图_16967556386348.png](doc/img/企业微信截图_16967556386348.png)

常规启动，耗时1.367s
![企业微信截图_16967555553218.png](doc/img/企业微信截图_16967555553218.png)


## 简单总结

1. native打包比较繁琐，步骤还不够流畅
2. native打包过程中，cpu、内存占用较大，打包耗时较长
3. native启动后占用资源较少，启动近乎无感
4. native打包后，包的体积稍大
5. native打包后，包运行不再依赖环境，类似docker镜像，可以直接运行，无需安装jdk、vs等环境

当前native还不适合广泛使用，devops还不够完善，可以尝鲜使用。


## 项目地址

[https://gitee.com/ruffianjiang/native-demo](https://gitee.com/ruffianjiang/native-demo)

## 参考

1. [Installation on Windows Platforms](https://docs.oracle.com/en/graalvm/jdk/17/docs/getting-started/installation-windows/#installation-on-windows-platforms)
2. [GitHub - spring-attic/spring-native: Spring Native is now superseded by Spring Boot 3 official native support](https://github.com/spring-attic/spring-native)
3. [GitHub - alexandreroman/whats-new-in-spring-boot-3: See what’s new in Spring Boot 3](https://github.com/alexandreroman/whats-new-in-spring-boot-3)

